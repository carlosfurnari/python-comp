'''
Created on 20 nov. 2017

@author: cfurnari
'''

import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

from gensim import corpora, models, similarities
dictionary = corpora.Dictionary.load('deerwester.dict')
corpus = corpora.MmCorpus('deerwester.mm') # comes from the first tutorial, "From strings to vectors"
print("-------------------------------------------")
print("-----------------  corpus  ----------------")
print("-------------------------------------------")
print(corpus)

lsi = models.LsiModel(corpus, id2word=dictionary, num_topics=2)
doc = "politic issue"
vec_bow = dictionary.doc2bow(doc.lower().split())
vec_lsi = lsi[vec_bow] # convert the query to LSI space
print("------------------  vector  lsi -----------")
print(vec_lsi)

index = similarities.MatrixSimilarity(lsi[corpus]) # transform corpus to LSI space and index it
index.save('deerwester.index')
index = similarities.MatrixSimilarity.load('deerwester.index')

sims = index[vec_lsi] # perform a similarity query against the corpus
print("------  (document_number, document_similarity)  -----")
print(list(enumerate(sims))) # print (document_number, document_similarity) 2-tuples

sims = sorted(enumerate(sims), key=lambda item: -item[1])
print("----------- ordenado por similaridad  ---------------")
print(sims) # print sorted (document number, similarity score) 2-tuples