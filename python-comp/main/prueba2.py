'''
Created on 23 nov. 2017

@author: cfurnari
'''
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

from gensim import corpora, models, similarities, matutils
import numpy as np

dictionaryLimpio = corpora.Dictionary(line.lower().split() for line in open('textos.txt'))
textsLimpio = [[word for word in line.lower().split()]
              for line in open('textos.txt')]
corpusLimpio = [dictionaryLimpio.doc2bow(text) for text in textsLimpio]

ldaLimpio = models.LdaModel(corpusLimpio, id2word=dictionaryLimpio, num_topics=100)
indextLdaLimpio = similarities.MatrixSimilarity(ldaLimpio[corpusLimpio])

doc1 = "Human computer interaction"
doc2 = "Human machine interface for lab abc computer applications"

vec_bow1 = dictionaryLimpio.doc2bow(doc1.lower().split())
vec_bow2 = dictionaryLimpio.doc2bow(doc2.lower().split())

vec_lda1 = ldaLimpio[vec_bow1]
vec_lda2 = ldaLimpio[vec_bow2]

dense1 = matutils.sparse2full(vec_lda1, ldaLimpio.num_topics)
dense2 = matutils.sparse2full(vec_lda2, ldaLimpio.num_topics)
sim = np.sqrt(0.5 * ((np.sqrt(dense1) - np.sqrt(dense2))**2).sum())
print(sim)