'''
Created on 23 nov. 2017

@author: cfurnari
'''
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

from gensim import corpora, models, similarities
from collections import defaultdict
from six import iteritems
from collections import defaultdict
from pprint import pprint  # pretty-printer

dictionaryLimpio = corpora.Dictionary(line.lower().split() for line in open('textos.txt'))
textsLimpio = [[word for word in line.lower().split()]
              for line in open('textos.txt')]

#se podria agregar otra stoplist
stoplist = set('for a of the and to in'.split())

textsSwOo1 = [[word for word in line.lower().split() if word not in stoplist]
         for line in open('textos.txt')]

frequency = defaultdict(int)
for text in textsSwOo1:
    for token in text:
        frequency[token] += 1

textssWoO1 = [[token for token in text if frequency[token] > 1]
         for text in textsSwOo1]

#pprint(textsLimpio)
#pprint(textssWoO1)

dictionarySwOo1 = corpora.Dictionary(textsSwOo1)

''' otra manera de crear el dictionarySwOo1
dictionarySwOo1 = corpora.Dictionary(line.lower().split() for line in open('textos.txt'))

stop_ids = [dictionarySwOo1.token2id[stopword] for stopword in stoplist
            if stopword in dictionarySwOo1.token2id]
once_ids = [tokenid for tokenid, docfreq in iteritems(dictionarySwOo1.dfs) if docfreq == 1]
dictionarySwOo1.filter_tokens(stop_ids + once_ids)
dictionarySwOo1.compactify()
'''

#print(dictionaryLimpio)
#print(dictionarySwOo1)

corpusLimpio = [dictionaryLimpio.doc2bow(text) for text in textsLimpio]
corpusSwOo1 = [dictionarySwOo1.doc2bow(text) for text in textsSwOo1]

tfidfLimpio = models.TfidfModel(corpusLimpio)
tfidfSwOo1 = models.TfidfModel(corpusSwOo1)

tfidLimpioNm = models.TfidfModel(corpusLimpio, normalize=True) # true: normalizacion euclidea del vector
tfidSwOo1Nm = models.TfidfModel(corpusSwOo1, normalize=True)

corpusTfidfLimpio = tfidLimpioNm[corpusLimpio]
corpusTfidfSwOo1 = tfidSwOo1Nm[corpusSwOo1]

lsiLimpio = models.LsiModel(corpusLimpio, id2word=dictionaryLimpio, num_topics=300) # valores recomendables entre 200 y 500
lsiSwOo1 = models.LsiModel(corpusSwOo1, id2word=dictionarySwOo1, num_topics=300)

lsiLimpioTfidf = models.LsiModel(corpusTfidfLimpio, id2word=dictionaryLimpio, num_topics=300) # valores recomendables entre 200 y 500
lsiSwOo1Tfidf = models.LsiModel(corpusTfidfSwOo1, id2word=dictionarySwOo1, num_topics=300)

rpLimpio = models.RpModel(corpusTfidfLimpio, num_topics=500)
rpSwOo1 = models.RpModel(corpusTfidfSwOo1, num_topics=500)

ldaLimpio = models.LdaModel(corpusLimpio, id2word=dictionaryLimpio, num_topics=100)
ldaSwOo1 = models.LdaModel(corpusSwOo1, id2word=dictionarySwOo1, num_topics=100)

hdpLimpio = models.HdpModel(corpusLimpio, id2word=dictionaryLimpio)
hdpSwOo1 = models.HdpModel(corpusSwOo1, id2word=dictionarySwOo1)

doc = "Human computer interaction"

vec_bowLimpio = dictionaryLimpio.doc2bow(doc.lower().split())
vec_bowSwOo1 = dictionarySwOo1.doc2bow(doc.lower().split())

# Tfidf
vec_tfidfLimpio = tfidfLimpio[vec_bowLimpio]
vec_tfidfSwOo1 = tfidfSwOo1[vec_bowSwOo1] 

indextfidfLimpio = similarities.MatrixSimilarity(tfidfLimpio[corpusLimpio]) # transform corpus to LSI space and index it
indextfidfSwOo1 = similarities.MatrixSimilarity(tfidfSwOo1[corpusSwOo1]) # transform corpus to LSI space and index it

simstfidfLimpio = indextfidfLimpio[vec_tfidfLimpio] # perform a similarity query against the corpus           
simstfidfSwOo1 = indextfidfSwOo1[vec_tfidfSwOo1] # perform a similarity query against the corpus    

simstfidfLimpio = sorted(enumerate(simstfidfLimpio), key=lambda item: -item[1])
simstfidfSwOo1 = sorted(enumerate(simstfidfSwOo1), key=lambda item: -item[1])

print("TFidf")
print(simstfidfLimpio)
print(simstfidfSwOo1)

# Tfidf Normalizado
vec_tfidfLimpioNm = tfidLimpioNm[vec_bowLimpio]
vec_tfidfSwOo1Nm = tfidSwOo1Nm[vec_bowSwOo1] 

indextfidfLimpioNm = similarities.MatrixSimilarity(tfidLimpioNm[corpusLimpio]) # transform corpus to LSI space and index it
indextfidfSwOo1Nm = similarities.MatrixSimilarity(tfidSwOo1Nm[corpusSwOo1]) # transform corpus to LSI space and index it

simstfidfLimpioNm = indextfidfLimpioNm[vec_tfidfLimpioNm] # perform a similarity query against the corpus           
simstfidfSwOo1Nm = indextfidfSwOo1Nm[vec_tfidfSwOo1Nm] # perform a similarity query against the corpus    

simstfidfLimpioNm = sorted(enumerate(simstfidfLimpioNm), key=lambda item: -item[1])
simstfidfSwOo1Nm = sorted(enumerate(simstfidfSwOo1Nm), key=lambda item: -item[1])

print("TFidf Normalizado")
print(simstfidfLimpioNm)
print(simstfidfSwOo1Nm)

# Lsi
vec_lsiLimpio = lsiLimpio[vec_bowLimpio] # convert the query to LSI space
vec_lsiSwOo1 = lsiSwOo1[vec_bowSwOo1] # convert the query to LSI space

indexLsiLimpio = similarities.MatrixSimilarity(lsiLimpio[corpusLimpio]) # transform corpus to LSI space and index it
indexLsiSwOo1 = similarities.MatrixSimilarity(lsiSwOo1[corpusSwOo1]) # transform corpus to LSI space and index it

simsLsiLimpio = indexLsiLimpio[vec_lsiLimpio] # perform a similarity query against the corpus           
simsLsiSwOo1 = indexLsiSwOo1[vec_lsiSwOo1] # perform a similarity query against the corpus    

simsLsiLimpio = sorted(enumerate(simsLsiLimpio), key=lambda item: -item[1])
simsLsiSwOo1 = sorted(enumerate(simsLsiSwOo1), key=lambda item: -item[1])

print("Lsi")
print(simsLsiLimpio)
print(simsLsiSwOo1)

# Lsi con Tfidf
vec_lsiTfidfLimpio = lsiLimpioTfidf[vec_bowLimpio] # convert the query to LSI space
vec_lsiTfidfSwOo1 = lsiSwOo1Tfidf[vec_bowSwOo1] # convert the query to LSI space

indexLsiTtfidfLimpio = similarities.MatrixSimilarity(lsiLimpioTfidf[corpusLimpio]) # transform corpus to LSI space and index it
indexLsiTtfidfSwOo1 = similarities.MatrixSimilarity(lsiSwOo1Tfidf[corpusSwOo1]) # transform corpus to LSI space and index it

simsLsiTtfidfLimpio = indexLsiTtfidfLimpio[vec_lsiTfidfLimpio] # perform a similarity query against the corpus           
simsLsiTtfidfSwOo1 = indexLsiTtfidfSwOo1[vec_lsiTfidfSwOo1] # perform a similarity query against the corpus    

simsLsiTtfidfLimpio = sorted(enumerate(simsLsiTtfidfLimpio), key=lambda item: -item[1])
simsLsiTtfidfSwOo1 = sorted(enumerate(simsLsiTtfidfSwOo1), key=lambda item: -item[1])

print("Lsi ttfidf")
print(simsLsiTtfidfLimpio)
print(simsLsiTtfidfSwOo1)

# Rp
vec_rpLimpio = rpLimpio[vec_bowLimpio]
vec_rpSwOo1 = rpSwOo1[vec_bowSwOo1] 

indextRpLimpio = similarities.MatrixSimilarity(rpLimpio[corpusLimpio]) # transform corpus to LSI space and index it
indextRpSwOo1 = similarities.MatrixSimilarity(rpSwOo1[corpusSwOo1]) # transform corpus to LSI space and index it

simsRpLimpio = indextRpLimpio[vec_rpLimpio] # perform a similarity query against the corpus           
simsRpSwOo1 = indextRpSwOo1[vec_rpSwOo1] # perform a similarity query against the corpus    

simsRpLimpio = sorted(enumerate(simsRpLimpio), key=lambda item: -item[1])
simsRpSwOo1 = sorted(enumerate(simsRpSwOo1), key=lambda item: -item[1])

print("Rp")
print(simsRpLimpio)
print(simsRpSwOo1)

# Lda
vec_ldaLimpio = ldaLimpio[vec_bowLimpio]
vec_ldaSwOo1 = ldaSwOo1[vec_bowSwOo1] 

indextLdaLimpio = similarities.MatrixSimilarity(ldaLimpio[corpusLimpio]) # transform corpus to LSI space and index it
indextLdaSwOo1 = similarities.MatrixSimilarity(ldaSwOo1[corpusSwOo1]) # transform corpus to LSI space and index it

simsLdaLimpio = indextLdaLimpio[vec_ldaLimpio] # perform a similarity query against the corpus           
simsLdaSwOo1 = indextLdaSwOo1[vec_ldaSwOo1] # perform a similarity query against the corpus    

simsLdaLimpio = sorted(enumerate(simsLdaLimpio), key=lambda item: -item[1])
simsLdaSwOo1 = sorted(enumerate(simsLdaSwOo1), key=lambda item: -item[1])

print("Lda")
print(simsLdaLimpio)
print(simsLdaSwOo1)

# Lda
vec_ldaLimpio = ldaLimpio[vec_bowLimpio]
vec_ldaSwOo1 = ldaSwOo1[vec_bowSwOo1] 

indextLdaLimpio = similarities.MatrixSimilarity(ldaLimpio[corpusLimpio]) # transform corpus to LSI space and index it
indextLdaSwOo1 = similarities.MatrixSimilarity(ldaSwOo1[corpusSwOo1]) # transform corpus to LSI space and index it

simsLdaLimpio = indextLdaLimpio[vec_ldaLimpio] # perform a similarity query against the corpus           
simsLdaSwOo1 = indextLdaSwOo1[vec_ldaSwOo1] # perform a similarity query against the corpus    

simsLdaLimpio = sorted(enumerate(simsLdaLimpio), key=lambda item: -item[1])
simsLdaSwOo1 = sorted(enumerate(simsLdaSwOo1), key=lambda item: -item[1])

print(simsLdaLimpio)
print(simsLdaSwOo1)